﻿/// <summary>
/// Message model.
/// </summary>
[System.Serializable]
public class MessageModel
{
    public string method;
    public string message;
}
