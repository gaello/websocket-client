# Implementing WebSocket Client in Unity

This repository contain a simple websocket client in Unity. You can check how it was created!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/11/11/implementing-websocket-in-unity/

Enjoy!

---

# How to use it?

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/websocket-client/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
